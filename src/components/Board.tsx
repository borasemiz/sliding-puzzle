import * as React from 'react';

import { Tile, TileDirections } from './Tile';
import { createRandomTileArray, calculateTilePosition, calculateTileDisplacement, checkTileCanMove } from '../utility/BoardUtilities';

import '../styles/Board.css';

const SHUFFLE_ITERATION_COUNT = 12;

interface IBoardProps {
  dimension: number;
  shuffleIterationCount?: number;
  onTilesOrdered: () => void;
  boardActive: boolean;
};

interface IBoardState {
  tiles: number[];
};

export class Board extends React.Component<IBoardProps, IBoardState> {
  private canClickTile: boolean = true;

  constructor(props: IBoardProps) {
    super(props);
    this.state = {
      tiles: createRandomTileArray(this.props.dimension, SHUFFLE_ITERATION_COUNT)
    };
  }

  public disable(): void {
    this.canClickTile = false;
  }

  public checkCompleteness(): boolean {
    return this.state.tiles.slice(0, this.state.tiles.length-1)
      .map((tileLabel, index) => tileLabel === index)
      .reduce((acc, curr) => acc = acc && curr);
  }

  public handleTileClick(index: number, tile: Tile): void {
    if (!this.canClickTile || !this.props.boardActive) return;
    this.moveTile(index, tile);
  }

  public moveTile(tileIndex: number, tile: Tile): void {
    const direction = checkTileCanMove(tileIndex, this.state.tiles.slice(), this.props.dimension);
    if (direction !== null) {
      this.canClickTile = false;
      const newTiles = this.updateTiles(tileIndex, direction);
      this.setState({
        tiles: newTiles
      }, () => this.onTileMoved());
    }
  }

  public onTileMoved(): void {
    this.canClickTile = true;
    if (this.checkCompleteness()) {
      this.props.onTilesOrdered();
    }
  }

  public updateTiles(tileIndex: number, direction: TileDirections): number[] {
    const newTiles = this.state.tiles.slice();
    const tempTile = newTiles[tileIndex];
    const displacement = calculateTileDisplacement(direction, this.props.dimension);
    newTiles[tileIndex] = newTiles[tileIndex+displacement];
    newTiles[tileIndex+displacement] = tempTile;
    return newTiles;
  }

  public renderTile(position: number, belongsTo: number, label: string) {
    return <Tile key={label}
      width={100 / this.props.dimension}
      height={100 / this.props.dimension}
      position={calculateTilePosition(position, this.props.dimension)} 
      belongsTo={calculateTilePosition(belongsTo, this.props.dimension)} 
      label={label} onClick={(tile) => this.handleTileClick(position, tile)} />;
  }

  public renderAllTiles() {
    return this.state.tiles
      .map((tile, index) => {
        if (tile === null) return tile;
        return this.renderTile(
          index, 
          tile, 
          (tile+1)+''
        );
      });
  }

  public render() {
    return (
      <div className="board">
        { this.renderAllTiles() }
      </div>
    );
  }
}
