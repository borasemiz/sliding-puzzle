import * as React from 'react';
import { Board } from './Board';

import '../styles/Game.css';

interface IGameProps {

};

interface IGameState {
  puzzleSolved: boolean;
};

export class Game extends React.Component<IGameProps, IGameState> {

  constructor(props: IGameProps) {
    super(props);
    this.state = {
      puzzleSolved: false
    };
  }

  public completeGame(): void {
    this.setState({
      puzzleSolved: true
    });
  }

  public render() {
    const wonMessage = this.state.puzzleSolved && <div className="won-message">you won!</div>;
    return (
      <div className="game">
        { wonMessage }
        <Board dimension={4} onTilesOrdered={() => {this.completeGame()}} boardActive={!this.state.puzzleSolved} />
      </div>
    );
  }
}