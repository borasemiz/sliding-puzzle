import * as React from 'react';
import { TILE_ANIMATION_DURATION } from '../settings';
import { calculateBackgroundPosition } from '../utility/TileUtilities';

import '../styles/Tile.css';

const TILE_BACKGROUND_SIZE = 600;

export enum TileDirections {
  LEFT,
  RIGHT,
  UP,
  DOWN
};

export interface ITilePosition {
  row: number;
  column: number;
};

export interface ITileProps {
  belongsTo: ITilePosition;
  height: number;
  label: string;
  width: number;
  position: ITilePosition;
  onClick: (tile: Tile) => void;
};

export interface ITileState {
  backgroundPosition: string;
  label: string;
  left: number;
  top: number;
};

export class Tile extends React.Component<ITileProps, ITileState> {
  constructor(props: ITileProps) {
    super(props);
  }

  public handleClick(): void {
    this.props.onClick(this);
  }

  public render() {
    return(
      <div className="tile" style={{
          top: `${this.calculateTop()}%`, 
          left: `${this.calculateLeft()}%`, 
          backgroundPosition: calculateBackgroundPosition(TILE_BACKGROUND_SIZE, this.props.belongsTo, this.props.width, this.props.height), 
          width: `${this.props.width}%`, 
          height: `${this.props.height}%`,
          transitionDuration: `${TILE_ANIMATION_DURATION}ms`}}
        onClick={() => this.handleClick()}>

        <div className="label">{this.props.label}</div>
      
      </div>
    );
  }

  private calculateTop(): number {
    return this.props.position.row * this.props.height;
  }

  private calculateLeft(): number {
    return this.props.position.column * this.props.width;
  }
}
