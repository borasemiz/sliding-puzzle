import * as React from 'react';
import '../styles/App.css';
import { Game } from './Game';

export class App extends React.Component<{}, {}> {
  public render() {
    return (
      <div className="app">
        <Game />
      </div>
    );
  }
}
