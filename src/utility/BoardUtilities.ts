import { TileDirections, ITilePosition } from "../components/Tile";

export const getDirectionFromReplacement = (boardDimension: number, displacement: number): TileDirections => {
  if (displacement === -1 * boardDimension)
    return TileDirections.UP;
  if (displacement === boardDimension)
    return TileDirections.DOWN;
  if (displacement === -1)
    return TileDirections.LEFT;
  if (displacement === 1)
    return TileDirections.RIGHT;
  throw new Error(`Cannot interpret direction with diplacement of ${displacement}`);
};

export const reverseDirection = (direction: TileDirections): TileDirections => {
  switch (direction) {
    case TileDirections.DOWN:
      return TileDirections.UP;
    case TileDirections.LEFT:
      return TileDirections.RIGHT;
    case TileDirections.RIGHT:
      return TileDirections.LEFT;
    case TileDirections.UP:
      return TileDirections.DOWN;
  }
};

export const createRandomTileArray = (dimension: number, shuffleIteration: number): number[] => {
  const orderedTiles: number[] = createOrderedTileArray(dimension);
  orderedTiles.push(null);
  return shuffleTiles(orderedTiles, dimension, shuffleIteration);
}

export const createOrderedTileArray = (dimension: number): number[] => {
  return new Array(Math.pow(dimension, 2) - 1).fill(0)
    .map((_, index) => index);
}

export const shuffleTiles = (tiles: number[], dimension: number, shuffleIteration: number): number[] => {
  let emptyTileIndex = tiles.indexOf(null);
  for (let i = 0; i < shuffleIteration * dimension; i++) {
    const tilesAroundEmptyTile = findEdgeTiles(tiles, emptyTileIndex, dimension);
    const selectedTileIndex = tilesAroundEmptyTile[Math.floor(tilesAroundEmptyTile.length * Math.random())];
    const temp = tiles[selectedTileIndex];
    tiles[selectedTileIndex] = tiles[emptyTileIndex];
    tiles[emptyTileIndex] = temp;
    emptyTileIndex = selectedTileIndex;
  }
  return tiles;
}

export const findEdgeTiles = (tiles: number[], emptyTileIndex: number, dimension: number): number[] => {
  return [
    (emptyTileIndex - 1) % dimension === dimension - 1 ? undefined : emptyTileIndex - 1,
    (emptyTileIndex + 1) % dimension === 0 ? undefined : emptyTileIndex + 1,
    emptyTileIndex + dimension,
    emptyTileIndex - dimension
  ].filter(v => typeof tiles[v] !== 'undefined');
}

export const calculateTilePosition = (tileIndex: number, dimension: number): ITilePosition => {
  return  {
    column: tileIndex % dimension,
    row: Math.floor(tileIndex / dimension )
  };
}

export const calculateTileDisplacement = (direction: TileDirections, boardDimension: number): number => {
  switch (direction) {
    case TileDirections.DOWN:
      return boardDimension;
    case TileDirections.UP:
      return -1 * boardDimension;
    case TileDirections.LEFT:
      return -1;
    case TileDirections.RIGHT:
      return 1;
  }
}

export const checkTileCanMove = (tileIndex: number, tileArray: number[], boardDimension: number): TileDirections => {
  if (tileArray[tileIndex - boardDimension] === null)
      return TileDirections.UP;
    if (tileArray[tileIndex + boardDimension] === null)
      return TileDirections.DOWN;
    if (tileIndex % boardDimension !== 0 && tileArray[tileIndex - 1] === null)
      return TileDirections.LEFT;
    if (tileIndex % boardDimension !== boardDimension - 1 && tileArray[tileIndex + 1] === null)
      return TileDirections.RIGHT;
    return null;
}