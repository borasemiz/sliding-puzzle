import { TileDirections, ITilePosition } from '../components/Tile';

export const moveTile = (direction: TileDirections, currentPosition: {
  top: number;
  left: number;
}, tileSize: { width: number; height: number }): typeof currentPosition => {
  switch (direction) {
    case TileDirections.LEFT:
      return {
        top: currentPosition.top,
        left: currentPosition.left - tileSize.width
      };
    case TileDirections.DOWN:
      return {
        top: currentPosition.top + tileSize.height,
        left: currentPosition.left
      };
    case TileDirections.UP:
      return {
        top: currentPosition.top - tileSize.height,
        left: currentPosition.left
      };
    case TileDirections.RIGHT:
      return {
        top: currentPosition.top,
        left: currentPosition.left + tileSize.width
      };
  }
}

export const calculateBackgroundPosition = (backgroundSize: number, tilePosition: ITilePosition, tileWidth: number, tileHeight: number): string => {
  return [
    -1 * tilePosition.column * (backgroundSize * tileWidth / 100),
    -1 * tilePosition.row * (backgroundSize * tileHeight / 100)
  ].map(value => `${value}px`).join(' ');
}